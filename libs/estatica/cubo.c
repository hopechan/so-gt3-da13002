#include <stdio.h>
#include "libstop.h"
/*
 * Calcula el cubo de un numero ingresado
 * --------------------------------------------
 * Funcion main()
 *  numero: Numero que se desea elevar al cubo
 *  numero_elevado: Resultado obtenido de la funcion cubo()
 * 
 * Funcion cubo(int numero)
 *  numero: numero a operar
 *  retorna: el numero entero luego de elevar al cubo el numero
*/

int cubo(int numero){
    int numero_al_cubo = numero * numero * numero;
    return numero_al_cubo;
}

int main(void)
{
    int numero;
    int numero_elevado;
    printf("Cubo \n");
    printf("Ingrese el numero entero que se desea elevar al cubo \n");
    scanf("%d", &numero);
    numero_elevado = cubo(numero);
    printf("El numero %d elevado al cubo es %d \n", numero, numero_elevado);
    return 0;
}