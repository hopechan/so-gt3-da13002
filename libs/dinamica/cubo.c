#include <stdio.h>
#include "libstop.h"
/*
 * Calcula el cubo de un numero ingresado
 * --------------------------------------------
 * Funcion cubo(int numero)
 *  numero: numero a operar
 *  retorna: el numero entero luego de elevar al cubo el numero
*/

int cubo(int numero){
    int numero_al_cubo = numero * numero * numero;
    return numero_al_cubo;
}