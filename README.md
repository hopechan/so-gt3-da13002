# Sistemas Operativos - Guia de Trabajo No 3

## Libreria Estatica
```bash
cd libs/estatica
gcc -c cubo.c -o cubo.o
gcc -c factorial.c -o factorial.o
ar -rv libstop.a cubo.o factorial.o
```

## Libreria Dinamica
```bash
cd libs/dinamica
gcc -c cubo.c -o cubo.o
gcc -c factorial.c -o factorial.o
ld -o libstop.so cubo.o factorial.o -shared
```

## Suma de dos numeros en GNU-Assembler

```bash
cd assembly
as --32 -o suma.o suma.s
ld --dynamic-linker /lib/ld-linux.so.2 -m elf_i386 -o suma -lc suma.o
```
