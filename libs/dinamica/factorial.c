#include <stdio.h>
#include "libstop.h"

/*
 * Calcula el factorial de un numero ingresado
 * --------------------------------------------
 * Funcion factorial(int numero)
 *  numero: numero a operar
 *  factorial: variable para guardar el calculo
 *  retorna: el numero factorial del numero
*/

int factorial(int numero){
    int factorial = 1;
    while (numero > 0){
        factorial = factorial*numero;
        numero = numero - 1;
    }
    return factorial;
}