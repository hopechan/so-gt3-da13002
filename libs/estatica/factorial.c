#include <stdio.h>
#include "libstop.h"

/*
 * Calcula el factorial de un numero ingresado
 * --------------------------------------------
 * Funcion main()
 *  numero: Numero que se desea calcular su factorial
 *  num_factorial: Resultado obtenido de la funcion factorial()
 * 
 * Funcion factorial(int numero)
 *  numero: numero a operar
 *  factorial: variable para guardar el calculo
 *  retorna: el numero factorial del numero
*/

int factorial(int numero){
    int factorial = 1;
    while (numero > 0){
        factorial = factorial*numero;
        numero = numero - 1;
    }
    return factorial;
}

int main(void){
    int numero;
    int num_factorial;
    printf("Factorial \n");
    printf("Ingrese el numero entero que se desea calcular el factorial: ");
    scanf("%d", &numero);
    num_factorial = factorial(numero);
    printf("El factorial de %d es %d \n", numero, num_factorial);
    return 0;
}
